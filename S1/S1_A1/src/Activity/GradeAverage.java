package Activity;
import java.util.Scanner;

public class GradeAverage {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        System.out.println("First Name: ");
        String firstName = myObj.nextLine();
        System.out.println("Last Name: ");
        String lastName = myObj.nextLine();
        System.out.println("First Subject Grade: ");
        double firstGrade = new Double(myObj.nextLine());
        System.out.println("Second Subject Grade: ");
        double secondGrade = new Double(myObj.nextLine());
        System.out.println("Third Subject Grade: ");
        double thirdGrade = new Double(myObj.nextLine());

        int average = (int)((firstGrade + secondGrade + thirdGrade) / 3);

        System.out.println("Good day, " + firstName + " " + lastName);
        System.out.println("Your grade average is:  " + average);
    }
}

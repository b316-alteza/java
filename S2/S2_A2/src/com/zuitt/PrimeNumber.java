package com.zuitt;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;


public class PrimeNumber {
    public static void main(String[] args) {
        int[] intPrime = {2, 3, 5, 7, 11};
        Scanner myObj = new Scanner(System.in);
        System.out.println("Input the nth prime you want to see: (1-5)");
        int idx = (myObj.nextInt()) -1;

        switch (idx){
            case 0:
                System.out.println("The first prime number is: " + intPrime[idx]);
                break;
            case 1:
                System.out.println("The second prime number is: " + intPrime[idx]);
                break;
            case 2:
                System.out.println("The third prime number is: " + intPrime[idx]);
                break;
            case 3:
                System.out.println("The fourth prime number is: " + intPrime[idx]);
                break;
            case 4:
                System.out.println("The fifth prime number is: " + intPrime[idx]);
                break;
            default:
                System.out.println("Invalid");
        }

        ArrayList<String> friends = new ArrayList<String>();

        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> toiletries = new HashMap<>();

        toiletries.put("toothpaste", 20);
        toiletries.put("toothbrush", 15);
        toiletries.put("soap", 12);

        System.out.println("Our current inventory is consist of: " + toiletries.toString());

    }
}

package com.zuitt;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        try {
            // Start of while loop solution
            System.out.println("Enter an integer whose factorial will be computed: ");
            int num = in.nextInt();
            int resFactorial = 1;

            if(num > 0) {
                System.out.print("The factorial of " + num + " is ");
                while(num > 1){
                    resFactorial *= num--;
                }
                System.out.println(resFactorial);
            } else if (num == 0) {
                System.out.println("The factorial of " + num + " is " + 1);
            } else {
                System.out.println("Factorial is only for non-negative integer");
            }
            // End of while loop solution

            // Start of for loop solution
            System.out.println("Enter an integer whose factorial will be computed: ");
            num = in.nextInt();
            resFactorial = 1;

            if(num > 0) {
                for(int i = num; i > 1; i--) {
                    resFactorial *= i;
                }
                System.out.println("The factorial of " + num + " is " + resFactorial);
            } else if (num == 0) {
                System.out.println("The factorial of " + num + " is " + 1);
            } else {
                System.out.println("Factorial is only for non-negative integer");
            }
            // End of for loop solution

            // Challenge
            System.out.println("Enter how many layers of pyramid do you want: (+int only)");
            num = in.nextInt();
            if(num > 0){
                for(int i = 1; i <= num; i++) {
                    for(int j = i; j > 0; j--){
                        System.out.print("* ");
                    }
                    System.out.println("");
                }
            } else {
                System.out.println( "Please input any positive integer only");
            }
        } catch (Exception e){//catch any errors
            System.out.println("Something went wrong!");
            e.printStackTrace();
        }
    }
}

package com.zuitt;

import java.util.Date;

public class Course {
    private String name, description;
    private int seat;
    private Double fee;
    private Date startDate, endDate;
    private User instructor;
    public Course(String name,String description, int seat, Double fee, Date startDate, Date endDate, User instructor){
        this.name = name;
        this.description = description;
        this.seat = seat;
        this.fee = fee;
        this.startDate = startDate;
        this.endDate = endDate;
        this.instructor = instructor;
    }
    public void setName(String nameParams){
        this.name = nameParams;
    }
    public String getName(){
        return this.name;
    }

    public void setDescription(String descriptionParams){
        this.description = descriptionParams;
    }
    public String getDescription(){
        return this.description;
    }

    public void setSeat(int seatParams){
        this.seat = seatParams;
    }
    public int getSeat(){
        return this.seat;
    }

    public void setFee(Double feeParams){
        this.fee = feeParams;
    }
    public Double getFee(){
        return this.fee;
    }

    public void setStartDate(Date startDateParams){
        this.startDate = startDateParams;
    }
    public Date getStartDate(){
        return this.startDate;
    }

    public void setEndDate(Date endDateParams){
        this.endDate = endDateParams;
    }
    public Date getEndDate(){
        return this.endDate;
    }

    public void setInstructor(User instructorParams){
        this.instructor = instructorParams;
    }
    public User getInstructor(){
        return this.instructor;
    }
}

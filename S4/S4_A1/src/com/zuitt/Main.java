package com.zuitt;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        User instructor = new User("Fear", "fear@mail.com", 24,"Tokyo, Japan");
        LocalDate localDate = LocalDate.of(2023, 7, 10);
        Date startDate = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        localDate = LocalDate.of(2023, 7, 14);
        Date endDate = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Course Java = new Course("Java", "Intense hack to learn Java from zero to Pro", 30, 61000.00, startDate, endDate, instructor);

        System.out.println("Hi! I am " + instructor.getName() + ". I am " + instructor.getAge() + " years old. You can reach me via my email: " + instructor.getEmail() + ". When I am off work, I can be found at my house in " + instructor.getAddress() + ".");

        System.out.println("Welcome to the course " + Java.getName() + ". This course can be described as " + Java.getDescription() + ". Your instructor for this course is Sir " + Java.getInstructor().getName() + ". Enjoy!");
    }
}

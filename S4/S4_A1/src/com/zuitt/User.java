package com.zuitt;

public class User {
    private String name, email, address;
    private int age;
    public User(String name,String email, int age, String address){
        this.name = name;
        this.email = email;
        this.age = age;
        this.address = address;
    }
    public void setName(String nameParams){
        this.name = nameParams;
    }
    public String getName(){
        return this.name;
    }

    public void setEmail(String emailParams){
        this.email = emailParams;
    }
    public String getEmail(){
        return this.email;
    }

    public void setAddress(String addressParams){
        this.address = addressParams;
    }
    public String getAddress(){
        return this.address;
    }

    public void setAge(int ageParams){
        this.age = ageParams;
    }
    public int getAge(){
        return this.age;
    }
}

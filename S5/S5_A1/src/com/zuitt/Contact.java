package com.zuitt;

public class Contact {
    private String name, contactNumber, address;

    public Contact(String name, String contactNumber, String address) {
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    public void addContactNumber(String contactNumber) {
        this.contactNumber += "\n" + contactNumber;
    }

    public void addAddress(String address) {
        this.address += "\n" + address;
    }

    public String getName() {
        return this.name;
    }
    public String getContactNumber() {
        return this.contactNumber;
    }
    public String getAddress() {
        return this.address;
    }
}

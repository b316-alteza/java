package com.zuitt;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();
        Contact fear = new Contact("Fear", "+819171234567", "my home is in Chiba, Japan");
        Contact phia = new Contact("Phia", "+639171234567", "my home is in Manila, Philippines");

        fear.addContactNumber("+819271234567");
        fear.addAddress("my work is in Tokyo, Japan");
        phia.addContactNumber("+639271234567");
        phia.addAddress("my work is in Quezon, Philippines");
        phonebook.add(fear);
        phonebook.add(phia);

        phonebook.showAll();
    }
}
